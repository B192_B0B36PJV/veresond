package com.thecherno.flappy.graphics;
//Copied form https://github.com/TheCherno/Flappy/tree/master/src/com/thecherno/flappy/graphics
import cz.cvut.fel.ocvj.shooter.utilities.BufferUtilities;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.InputStream;

import static org.lwjgl.opengl.GL11.*;

public class Texture {

    public static final int shiftColorChannelBy3Spaces = 3*8;
    public static final int shiftColorChannelBy2Spaces = 2*8;
    public static final int shiftColorChannelBy1Space = 8;
    private int width, height;
    private int texture;

    public Texture(String path) {
        texture = load(path);
    }

    private int load(String path) {
        int[] pixels = null;
        try {
            ClassLoader classloader = Thread.currentThread().getContextClassLoader();
            InputStream is = classloader.getResourceAsStream(path);
            BufferedImage image = ImageIO.read(is);

            width = image.getWidth();
            height = image.getHeight();
            pixels = new int[width * height];
            image.getRGB(0, 0, width, height, pixels, 0, width);
        } catch (IOException e) {
            throw new java.lang.Error("Texture load failed", e);
        }

        /*Color channels in pixels are stored in int as 0xAARRGGBB but opengl requires format 0xAABBGGRR.
        Therefore we need to do bit masking and bit shifting to match the format.*/

        int[] data = new int[width * height];
        for (int i = 0; i < width * height; i++) {
            int a = (pixels[i] & 0xff000000) >> shiftColorChannelBy3Spaces;
            int r = (pixels[i] & 0x00ff0000) >> shiftColorChannelBy2Spaces;
            int g = (pixels[i] & 0x0000ff00) >> shiftColorChannelBy1Space;
            int b = (pixels[i] & 0x000000ff);

            data[i] = a << shiftColorChannelBy3Spaces | b << shiftColorChannelBy2Spaces | g << shiftColorChannelBy1Space | r;
        }

        int result = glGenTextures();
        glBindTexture(GL_TEXTURE_2D, result);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, BufferUtilities.createNewIntBuffer(data));
        glBindTexture(GL_TEXTURE_2D, 0);
        return result;
    }

    /**
     * Binds texture to a texturing target
     */
    public void bind() {
        glBindTexture(GL_TEXTURE_2D, texture);
    }

    /**
     * Unbinds texture
     */
    public void unbind() {
        glBindTexture(GL_TEXTURE_2D, 0);
    }

}