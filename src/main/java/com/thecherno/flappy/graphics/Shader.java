package com.thecherno.flappy.graphics;
//Copied form https://github.com/TheCherno/Flappy/tree/master/src/com/thecherno/flappy/graphics

import cz.cvut.fel.ocvj.shooter.math.Matrix4x4;
import cz.cvut.fel.ocvj.shooter.utilities.ShaderUtilities;

import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.lwjgl.opengl.GL20.*;

public class Shader {

    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);
    private static final int VERTEX_ATTRIBUTE = 0;
    private static final int TEXTURE_COORDINATES_ATTRIBUTE = 1;

    public static int getVertexAttribute() {
        return VERTEX_ATTRIBUTE;
    }

    public static int getTextureCoordinatesAttribute() {
        return TEXTURE_COORDINATES_ATTRIBUTE;
    }

    public static Shader getGameobjectShader() {
        return gameobject;
    }

    private static Shader gameobject;

    private boolean enabled = false;

    private final int ID;
    private Map<String, Integer> locationCache = new HashMap<String, Integer>();

    /**
     * Generates shader
     * @param vertex path to vertex shader
     * @param fragment path to fragment shader
     */
    public Shader(String vertex, String fragment) {
        ID = ShaderUtilities.generateShader(vertex, fragment);
    }

    /**
     * Loads all shaders, in our case we have only one
     */
    public static void loadShaders() {

         gameobject = new Shader("shaders/gameobject.vert", "shaders/gameobject.frag");
    }

    /**
     * Access to shader uniform variables
     * @param name name of uniform variable
     * @return  returns uniform variable from Shader
     */
    public int getUniform(String name) {
        if (locationCache.containsKey(name))
            return locationCache.get(name);

        int result = glGetUniformLocation(ID, name);
        if (result == -1)
            LOGGER.log(Level.SEVERE, "Could not find uniform variable: ", name);
        else
            locationCache.put(name, result);
        return result;
    }

    /**
     * Sets int uniform int value to the shader
     * @param name name of the shader uniform value
     * @param value new value of the shader uniform value
     */
    public void setUniform1i(String name, int value) {
        if (!enabled) enable();
        glUniform1i(getUniform(name), value);
    }
    /**
     * Sets int uniform float value to the shader
     * @param name name of the shader uniform value
     * @param value new value of the shader uniform value
     */
    public void setUniform1f(String name, float value) {
        if (!enabled) enable();
        glUniform1f(getUniform(name), value);
    }

    /**
     * Sets float Matrix4x4 to the shader uniform value
     * @param name name of the shader uniform value
     * @param matrix new matrix
     */
    public void setUniformMat4f(String name, Matrix4x4 matrix) {
        if (!enabled) enable();
        glUniformMatrix4fv(getUniform(name), false, matrix.toFloatBuffer());
    }

    /**
     * Enables the shader
     */
    public void enable() {
        glUseProgram(ID);
        enabled = true;
    }
    /**
     * Disables the shader
     */
    public void disable() {
        glUseProgram(0);
        enabled = false;
    }
}