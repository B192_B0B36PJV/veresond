package com.thecherno.flappy.graphics;
//Copied form https://github.com/TheCherno/Flappy/tree/master/src/com/thecherno/flappy/graphics
import cz.cvut.fel.ocvj.shooter.utilities.BufferUtilities;

import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL15.*;
import static org.lwjgl.opengl.GL20.glEnableVertexAttribArray;
import static org.lwjgl.opengl.GL20.glVertexAttribPointer;
import static org.lwjgl.opengl.GL30.glBindVertexArray;
import static org.lwjgl.opengl.GL30.glGenVertexArrays;

public class VertexArray {

    private int vertexArrayObject, vertexBufferObject, indicesBufferObject, textureCoordinatesBufferObject;
    private int count;


    /**
     * Creates openGL program for rendering an object
     * @param vertices points of the mesh
     * @param indices instructions for creating triangles from vertices
     * @param textureCoordinates instructions for mapping the texture on the mesh
     */
    public VertexArray(float[] vertices, byte[] indices, float[] textureCoordinates) {
        count = indices.length;

        vertexArrayObject = glGenVertexArrays();
        glBindVertexArray(vertexArrayObject);

        vertexBufferObject = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, vertexBufferObject);
        glBufferData(GL_ARRAY_BUFFER, BufferUtilities.createNewFloatBuffer(vertices), GL_STATIC_DRAW);
        glVertexAttribPointer(Shader.getVertexAttribute(), 3, GL_FLOAT, false, 0, 0);
        glEnableVertexAttribArray(Shader.getVertexAttribute());

        textureCoordinatesBufferObject = glGenBuffers();
        glBindBuffer(GL_ARRAY_BUFFER, textureCoordinatesBufferObject);
        glBufferData(GL_ARRAY_BUFFER, BufferUtilities.createNewFloatBuffer(textureCoordinates), GL_STATIC_DRAW);
        glVertexAttribPointer(Shader.getTextureCoordinatesAttribute(), 2, GL_FLOAT, false, 0, 0);
        glEnableVertexAttribArray(Shader.getTextureCoordinatesAttribute());

        indicesBufferObject = glGenBuffers();
        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesBufferObject);
        glBufferData(GL_ELEMENT_ARRAY_BUFFER, BufferUtilities.createNewByteBuffer(indices), GL_STATIC_DRAW);

        glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
        glBindBuffer(GL_ARRAY_BUFFER, 0);
        glBindVertexArray(0);
    }

    /**
     * binds a vertex array object
     */
    public void bind() {
        glBindVertexArray(vertexArrayObject);
        if (indicesBufferObject > 0)
            glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, indicesBufferObject);
    }


    /**
     * Draws a vertex array object
     */
    public void draw() {
        if (indicesBufferObject > 0)
            glDrawElements(GL_TRIANGLES, count, GL_UNSIGNED_BYTE, 0);
        else
            glDrawArrays(GL_TRIANGLES, 0, count);
    }

    /**
     * Binds and draws a vertex array object
     */
    public void render() {
        bind();
        draw();
    }

}