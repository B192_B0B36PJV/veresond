package cz.cvut.fel.ocvj.shooter.math;

public class Vector3 {
    public float getX() {
        return x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public float getZ() {
        return z;
    }

    public void setZ(float z) {
        this.z = z;
    }

    private float x,y,z;

    /**
     * Constructs an instance of Vector3 with all three components set to 0
     */
    public Vector3() {
        this.x = 0;
        this.y = 0;
        this.z = 0;
    }

    /**
     * Constructs an instance of Vector3 with custom x, y components
     * setting the z component to 0
     * @param x the horizontal component
     * @param y the vertical component
     */
    public Vector3(float x, float y) {
        this.x = x;
        this.y = y;
        this.z = 0;
    }

    /**
     * Constructs an instance of Vector3 with custom x, y, z components
     * @param x the horizontal component
     * @param y the vertical component
     * @param z the depth component
     */
    public Vector3(float x, float y, float z) {
        this.x = x;
        this.y = y;
        this.z = z;
    }

    /**
     * Normalizes this instance of Vector3 to be a unit vector
     */
    public void normalize() {
        if (this.x < -1) this.x = -1;
        if (this.x > 1) this.x = 1;
        if (this.y < -1) this.y = -1;
        if (this.y > 1) this.y = 1;
    }

    /**
     * Returns an instance of Vector3 with the default direction
     * game objects should be facing
     * @return an instance of Vector3 with the default direction
     */
    public static Vector3 getDefaultDirection() {
        return new Vector3(0f, 1f);
    }

    /**
     * Checks if vectors are equal
     * @param o
     * @return
     */
    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Vector3 vector3 = (Vector3) o;
        return Float.compare(vector3.x, x) == 0 &&
                Float.compare(vector3.y, y) == 0 &&
                Float.compare(vector3.z, z) == 0;
    }

    /**
     * Return the resulting Vector3 of multiplying a given vector by a given scalar
     * @param v the vector to be multiplied
     * @param m the scalar to multiply by
     * @return the resulting Vector3
     */
    public static Vector3 multiply(Vector3 v, float m)
    {
        return new Vector3(m*v.x,m*v.y,m*v.z);
    }

    /**
     * Return the resulting Vector3 of adding two vectors together
     * @param a the first vector
     * @param b the second vector
     * @return the resulting sum of the two vectors
     */
    public static Vector3 add(Vector3 a, Vector3 b)
    {
        return new Vector3(a.x+b.x,a.y+b.y,a.z+b.z);
    }
}
