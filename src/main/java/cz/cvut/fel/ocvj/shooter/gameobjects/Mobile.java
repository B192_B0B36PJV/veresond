package cz.cvut.fel.ocvj.shooter.gameobjects;

import cz.cvut.fel.ocvj.shooter.math.Vector3;

public class Mobile extends GameObject {
    private float speed;
    private Vector3 rawDelta;

    /**
     * Constructs an instance of Mobile
     * @param pathToTexture a relative path to the texture image
     * @param position starting position in the game world
     * @param size size of the instance in the game world
     * @param speed movement speed in the game world
     */
    public Mobile(String pathToTexture, Vector3 position, float size, float speed) {
        super(pathToTexture, position, size);
        this.speed = speed;
    }

    public float getRawDeltaX() { return this.rawDelta.getX(); }
    public float getRawDeltaY() { return this.rawDelta.getY(); }

    public void setRawDelta(Vector3 newRawDelta) {
        newRawDelta.normalize();
        this.rawDelta = newRawDelta;
    }

    /**
     * Change the position in the game world,
     * depending on the rawDelta and speed.
     * Also rotate the instance to face the same direction
     * as rawDelta.
     */
    public void move() {
        if (rawDelta.getX() != 0 || rawDelta.getY() != 0) {
            this.direction = rawDelta;
        }
        this.position = Vector3.add(this.position, Vector3.multiply(this.rawDelta,this.speed));

    }
}
