package cz.cvut.fel.ocvj.shooter.gameobjects;

import cz.cvut.fel.ocvj.shooter.math.Vector3;

public abstract class GUI extends GameObject {
    private Boolean active;
    private Vector3 offset = new Vector3(0,0,0);
    private Player player;


    public void setActive(Boolean active) {
        this.active = active;
    }
    public Boolean getActive() {
         return this.active;
    }

    /**
     * Creates an instance of GUI
     * @param pathToTexture a relative path to the texture image
     * @param position starting position in the game world
     * @param size size of the instance in the game world
     * @param player reference to Player
     */
    public GUI(String pathToTexture, Vector3 position, float size, Player player) {
        super(pathToTexture, position, size);
        this.player = player;
        offset = this.position;
        active = true;
    }

    /**
     * Renders this object to the screen only if it's active.
     */
    @Override
    public void render() {
        if(active)
        super.render();
    }

    /**
     * Updates the position of the instance in the game world
     * based on the player position and the offset from the player.
     */
    public void update(){
        this.position = new Vector3(
                this.player.getPosition().getX() + offset.getX(),
                this.player.getPosition().getY() + offset.getY(),
                this.position.getZ());
    }

    protected abstract float[] generateVertices(float size);
}
