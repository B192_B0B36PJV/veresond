package cz.cvut.fel.ocvj.shooter.utilities;

import org.lwjgl.BufferUtils;

import java.nio.ByteBuffer;
import java.nio.FloatBuffer;
import java.nio.IntBuffer;
/**
 * Packs array into a buffer, this is helpful for sending data to to OpenGL
 * @author TheCherno
 * @see <a href="https://github.com/TheCherno/Flappy/blob/master/src/com/thecherno/flappy/utils/BufferUtils.java">original</a>
 */
public class BufferUtilities {

    /**
     * Pack a integer array into a buffer
     * @param values int array
     * @return int buffer
     */
   public static IntBuffer createNewIntBuffer(int[] values)
    {
        IntBuffer ret = BufferUtils.createIntBuffer(values.length);
        ret.put(values).flip();
        return ret;
    }

    /**
     * Pack a float array into a buffer
     * @param values float array
     * @return float buffer
     */
    public static FloatBuffer createNewFloatBuffer(float[] values)
    {
        FloatBuffer ret = BufferUtils.createFloatBuffer(values.length);
        ret.put(values).flip();
        return ret;
    }

    /**
     * Pack a byte array into a buffer
     * @param values byte array
     * @return byte buffer
     */
    public static ByteBuffer createNewByteBuffer(byte[] values)
    {
        ByteBuffer ret = BufferUtils.createByteBuffer(values.length);
        ret.put(values).flip();
        return ret;
    }
}
