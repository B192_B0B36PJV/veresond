package cz.cvut.fel.ocvj.shooter.gameobjects;

import cz.cvut.fel.ocvj.shooter.input.Input;
import cz.cvut.fel.ocvj.shooter.math.Vector3;

import static org.lwjgl.glfw.GLFW.*;

public class Player extends Creature {
    private static final int maxHealth = 3;

    public static int getMaxHealth() {
        return maxHealth;
    }

    private Boolean hasKey = false;

    public Boolean getHasKey() {
        return hasKey;
    }

    public void setHasKey(Boolean hasKey) {
        this.hasKey = hasKey;
    }

    /**
     * Constructs an instance of Player
     * @param pathToTexture a relative path to the texture image
     * @param position starting position in the game world
     * @param size size of the instance in the game world
     * @param speed movement speed in the game world
     * @param hp health points indicating how many hits the Enemy can take before dying
     * @param reloadDuration interval in between shots
     */
    public Player(String pathToTexture, Vector3 position, float size, float speed, int hp, int reloadDuration) {
        super(pathToTexture, position, size, speed, hp, reloadDuration);
    }

    /**
     * Calculate the rawDelta (direction of movement) based on
     * what keys the player is pressing.
     * @return direction of movement in the game world
     */
    private Vector3 calculateRawDelta() {
        Vector3 rawDelta = new Vector3();
        if (Input.isKeyDown(GLFW_KEY_LEFT)) {
            rawDelta.setX(-1);
        } else if (Input.isKeyDown(GLFW_KEY_RIGHT)) {
            rawDelta.setX(1);
        } else if (Input.isKeyDown(GLFW_KEY_UP)) {
            rawDelta.setY(1);
        } else if (Input.isKeyDown(GLFW_KEY_DOWN)) {
            rawDelta.setY(-1);
        }
        return rawDelta;
    }

    /**
     * Returns whether the player is currently shooting.
     * @return whether the player is currently shooting
     */
    public boolean isShooting() {
        if (Input.isKeyDown(GLFW_KEY_SPACE)) {
            return canShoot();
        } else {
            return false;
        }
    }

    /**
     * Updates the Player's movement direction and its
     * position in the game world.
     */
    public void update() {
        this.setRawDelta(this.calculateRawDelta());
        this.move();
    }
}
