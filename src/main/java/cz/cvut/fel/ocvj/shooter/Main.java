package cz.cvut.fel.ocvj.shooter;

import com.thecherno.flappy.graphics.Shader;
import cz.cvut.fel.ocvj.shooter.gameobjects.World;
import cz.cvut.fel.ocvj.shooter.input.Input;
import cz.cvut.fel.ocvj.shooter.math.Matrix4x4;
import cz.cvut.fel.ocvj.shooter.math.Vector3;
import cz.cvut.fel.ocvj.shooter.utilities.JsonUtilities;
import org.lwjgl.glfw.GLFWVidMode;
import org.lwjgl.opengl.GL;

import java.util.logging.Level;
import java.util.logging.Logger;

import static org.lwjgl.glfw.GLFW.*;
import static org.lwjgl.opengl.GL11.*;
import static org.lwjgl.opengl.GL13.GL_TEXTURE1;
import static org.lwjgl.opengl.GL13.glActiveTexture;
import static org.lwjgl.system.MemoryUtil.NULL;

public class Main implements Runnable{
    private Main(){}
    private static Main main;
    public static Main getMain()
    {
        if (main == null)
            main = new Main();

        return main;
    }
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    /**
     * Number of nanoseconds between frames
     */
    private final static double NANOS_BETWEEN_FRAMES = 1e9 / 60.0;

    private static int width = 1280;
    private static int height =720;
    private static Thread thread;
    private static boolean running = false;

    private static long window;

    public static void setRunning(boolean running) {
        Main.running = running;
    }

    public static Thread getThread() {
        return thread;
    }

    private static String pathToMap = "save.json";
    private static World world;

    /**
     * Creates a new thread for the game loop if it is not already running
     */
    public void start(){
        if(!running)
        thread = new Thread(this,"Game");
        running = true;
        thread.start();
    }

    /**
     * Initializes glfw window, shaders and World tiles
     */
    private void init(){
        if(!glfwInit())
        {
            LOGGER.log(Level.SEVERE, "GLFW wasn't initialized");
        }
        glfwWindowHint(GLFW_RESIZABLE, GL_TRUE);
        //window = glfwCreateWindow(width, height,"Shooter",glfwGetPrimaryMonitor(), NULL); uncomment for fullscreen
        window = glfwCreateWindow(width, height,"Shooter",NULL, NULL);

        GLFWVidMode vidmode = glfwGetVideoMode(glfwGetPrimaryMonitor());
        assert vidmode != null;
        glfwSetWindowPos(window,(vidmode.width() - width)/2,(vidmode.height() - height)/2);

        glfwMakeContextCurrent(window);
        glfwShowWindow(window);

        GL.createCapabilities();
        glfwSwapInterval(1);//VSYNC
        glClearColor(0.082f,0.424f,0.60f,1.0f);

        glEnable(GL_DEPTH_TEST);
        glActiveTexture(GL_TEXTURE1);

        Shader.loadShaders();

        glfwSetKeyCallback(window, new Input());

        Matrix4x4 projectionMatrix = Matrix4x4.orthographic(-10.0f,10.0f,-10.0f *9.0f/16.0f,10.0f *9.0f/16.0f,-1.0f, 1.0f);
        Shader.getGameobjectShader().setUniformMat4f("pr_matrix",projectionMatrix);
        Shader.getGameobjectShader().setUniform1i("tex",1);

        world =  World.getWorld(pathToMap);
    }

    /**
     * Main game loop function that updates and renders all GameObjects
     * @see <a href="https://github.com/TheCherno/Flappy/blob/master/src/com/thecherno/flappy/Main.java">inspiration</a>
     */
    public void run(){
        init();

        long lastTime = System.nanoTime();
        double delta = 0.0;
        long timer = System.currentTimeMillis();
        int updates = 0;
        int frames = 0;
        while (running) {
            long now = System.nanoTime();
            delta += (now - lastTime) / NANOS_BETWEEN_FRAMES;
            lastTime = now;
            if (delta >= 1.0) {
                try {
                    update();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                updates++;
                delta--;
            }
            render();
            frames++;
            if (System.currentTimeMillis() - timer > 1000) {
                timer += 1000;
                LOGGER.log(Level.INFO, updates + " ups, " + frames + " fps");
                updates = 0;
                frames = 0;
            }
            if (glfwWindowShouldClose(window))
                running = false;
        }

        glfwDestroyWindow(window);
        glfwTerminate();
    }

    private void update() throws InterruptedException {
        glfwPollEvents();
        world.update();
        if(world.getIsTimeForRestart())
        {
            world = world.resetWorld(pathToMap);
        }
    }
    private void render(){
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
        world.render();

        int error = glGetError();
        if(error != GL_NO_ERROR)
        {
            LOGGER.log(Level.SEVERE, "GLFW wasn't initialized", error);
        }
        glfwSwapBuffers(window);
    }

    /**
     * Initiates the game world layout and saves it to JSON
     * and starts the program
     * @param args command-line arguments
     */
    public static void main(String[] args) {
        try {
            pathToMap = args[0];
        } catch (ArrayIndexOutOfBoundsException e) {
            LOGGER.log(Level.WARNING, "Missing command-line argument, using default map.");
        }
        int[] ar = {5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,
                5,5,5,11,2,2,2,2,2,2,2,10,5,5,5,5,5,5,5,5,5,
                5,11,3,0,0,0,0,0,0,0,1,2,2,2,2,2,2,10,5,5,
                5,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,5,5,
                5,6,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,4,5,5,
                5,6,0,0,7,8,18,18,8,8,8,8,8,8,8,18,18,12,5,5,
                5,6,0,7,12,5,16,16,5,5,5,5,5,5,5,16,16,5,5,5,
                5,13,8,12,5,11,17,17,2,2,2,10,5,5,5,16,16,5,5,5,
                5,5,5,5,11,3,0,0,0,0,0,4,5,5,11,17,17,2,10,5,
                5,5,5,11,3,-3,0,0,0,0,0,4,5,11,3,0,0,0,4,5,
                5,5,11,3,0,0,0,0,0,0,0,4,5,6,0,0,0,0,4,5,
                5,11,3,0,0,0,0,0,0,0,0,4,5,13,9,0,0,0,4,5,
                5,6,0,0,0,0,0,0,0,0,0,1,10,5,13,9,0,-2,4,5,
                5,6,0,0,0,0,0,0,0,0,0,0,1,10,5,13,9,0,4,5,
                5,13,8,9,0,-3,0,0,0,0,0,0,0,4,5,5,13,8,12,5,
                5,5,5,13,9,0,0,0,0,0,-1,0,0,4,5,5,5,5,5,5,
                5,5,5,5,13,8,8,9,0,0,0,0,0,4,5,5,5,5,5,5,
                5,5,5,5,5,5,5,13,8,9,0,0,7,12,5,5,5,5,5,5,
                5,5,5,5,5,5,5,5,5,13,8,8,12,5,5,5,5,5,5,5,
                5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5,5};
        Boolean hasKey= false;
        int hp = 1;
        Vector3 playerPosition = new Vector3();
        Vector3[] enemyPositions = {new Vector3(4,4),new Vector3(-4,-4),new Vector3(10,10), new Vector3(-10,-10) };
        JsonUtilities.saveJson(JsonUtilities.generateJson(ar,20,20,hasKey, hp,playerPosition,enemyPositions),"save.json");
        Main.getMain().start();
    }
}
