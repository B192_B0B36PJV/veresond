package cz.cvut.fel.ocvj.shooter.gameobjects;

import com.thecherno.flappy.graphics.Shader;
import com.thecherno.flappy.graphics.Texture;
import com.thecherno.flappy.graphics.VertexArray;
import cz.cvut.fel.ocvj.shooter.math.Matrix4x4;
import cz.cvut.fel.ocvj.shooter.math.Vector3;

public class GameObject {

    protected float size;
    private VertexArray mesh;
    private Texture texture;

    protected Vector3 position;
    protected Vector3 direction = Vector3.getDefaultDirection();

    /**
     * Creates a GameObject
     * @param pathToTexture
     * @param position
     * @param size
     */
    public GameObject(String pathToTexture, Vector3 position, float size) {
        this.position = position;
        this.size = size;
        float[] vertices = generateVertices(size); //declare the position of individual vertices of the mesh

        byte[] indices = generateIndices(); //indices create triangles from vertices

        float[] textureCoordinatescs = generateTextureCoordinates(); //texture coordinates map textures to vertices

        mesh = new VertexArray(vertices, indices, textureCoordinatescs);
        texture = new Texture(pathToTexture);
    }
    //Generates a square with the length of size
    protected float[] generateVertices(float size){
        return new float[]{
                -size / 2.0f, -size / 2.0f, 0.0f, //vertex number 0, bottom left vertex
                -size / 2.0f, size / 2.0f, 0.0f, //vertex number 1, top left vertex
                size / 2.0f, size / 2.0f, 0.0f, //vertex number 2, top right vertex
                size / 2.0f, -size / 2.0f, 0.0f //vertex number 3, bottom right vertex
        };
    }

    private static byte[] generateIndices(){
       return new byte[]{
                0, 1, 2, //triangle from vertices number 0, 1 and 2
                2, 3, 0 //triangle from vertices number 0, 1 and 2
        };
    }

    private static float[] generateTextureCoordinates()
    {
        return new float[]{
                0, 1, //place top left corner of texture to vertex number 0
                0, 0, //place bottom left corner of texture to vertex number 1
                1, 0, //place bottom right corner of texture to vertex number 2
                1, 1  //place top right corner of texture to vertex number 3
        };
    }
    public Vector3 getPosition() {
        return position;
    }

    public float getSize() {
        return size;
    }

    /**
     * renders a gameobject
     */
    public void render(){
        texture.bind();
        Shader.getGameobjectShader().enable();
        Shader.getGameobjectShader().setUniformMat4f("ml_matrix", Matrix4x4.translationFromVector(position).multiply(Matrix4x4.rotationFromVector(direction)));
        mesh.render();
        Shader.getGameobjectShader().disable();
        texture.unbind();
    }
}
