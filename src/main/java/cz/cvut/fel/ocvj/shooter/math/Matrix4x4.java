package cz.cvut.fel.ocvj.shooter.math;

import cz.cvut.fel.ocvj.shooter.utilities.BufferUtilities;

import java.nio.FloatBuffer;
import java.util.Arrays;

public class Matrix4x4 {

    public static final int matrixSize = 16;
    public static final int numberOfRowsOrCollums = 4;

    public Matrix4x4(float[] values) {
        this.values = values;
    }

    public Matrix4x4() {
        Arrays.fill(this.values, 0);
    }

    private float[] values = new float[matrixSize];

    public float[] getValues() {
        return values;
    }

    /**
     * Returns a 4 by 4 identity matrix
     * @author TheCherno
     * @return a 4 by 4 identity matrix
     * @see <a href="https://github.com/TheCherno/Flappy/blob/master/src/com/thecherno/flappy/maths/Matrix4f.java">original</a>
     */
    public static Matrix4x4 identity() {
        Matrix4x4 result = new Matrix4x4();
        for (int i = 0; i < matrixSize; i++) {
            result.values[i] = 0.0f;
        }
        result.values[0 + 0 * numberOfRowsOrCollums] = 1.0f;
        result.values[1 + 1 * numberOfRowsOrCollums] = 1.0f;
        result.values[2 + 2 * numberOfRowsOrCollums] = 1.0f;
        result.values[3 + 3 * numberOfRowsOrCollums] = 1.0f;

        return result;
    }

    /**
     * Returns a matrix representing a translation in space
     * based on the translational vector provided
     * @author TheCherno
     * @param vector translation in space in form of vector
     * @return matrix representing the translation
     * @see <a href="https://github.com/TheCherno/Flappy/blob/master/src/com/thecherno/flappy/maths/Matrix4f.java">original</a>
     */
    public static Matrix4x4 translationFromVector(Vector3 vector) {
        Matrix4x4 result = identity();
        result.values[0 + 3 * numberOfRowsOrCollums] = vector.getX();
        result.values[1 + 3 * numberOfRowsOrCollums] = vector.getY();
        result.values[2 + 3 * numberOfRowsOrCollums] = vector.getZ();
        return result;
    }

    /**
     * Returns a matrix representing a rotation in space
     * based on the angle vector provided,
     * @author TheCherno
     * @param angle angle by which to rotate
     * @return matrix representing the rotation
     * @see <a href="https://github.com/TheCherno/Flappy/blob/master/src/com/thecherno/flappy/maths/Matrix4f.java">original</a>
     */
    public static Matrix4x4 rotationFromVector(Vector3 angle) {
        Matrix4x4 result = identity();

        result.values[0 + 0 * numberOfRowsOrCollums] = angle.getY();
        result.values[1 + 0 * numberOfRowsOrCollums] = -angle.getX();

        result.values[0 + 1 * numberOfRowsOrCollums] = angle.getX();
        result.values[1 + 1 * numberOfRowsOrCollums] = angle.getY();

        return result;
    }

    /**
     * Multiplies this instance by a given matrix
     * @author TheCherno
     * @param matrix by which to multiply
     * @return resulting matrix of the multiplication
     * @see <a href="https://github.com/TheCherno/Flappy/blob/master/src/com/thecherno/flappy/maths/Matrix4f.java">original</a>
     */
    public Matrix4x4 multiply(Matrix4x4 matrix) {
        Matrix4x4 result = new Matrix4x4();
        for (int y = 0; y < numberOfRowsOrCollums; y++) {
            for (int x = 0; x < numberOfRowsOrCollums; x++) {
                float sum = 0.0f;
                for (int e = 0; e < numberOfRowsOrCollums; e++) {
                    sum += this.values[x + e * numberOfRowsOrCollums] * matrix.values[e + y * numberOfRowsOrCollums];
                }
                result.values[x + y * numberOfRowsOrCollums] = sum;
            }
        }
        return result;
    }

    /**
     * Returns a matrix for orthographic projection
     * @author TheCherno
     * @param left the left most point to be rendered
     * @param right the right most point to be rendered
     * @param bottom the bottom most point to be rendered
     * @param top the top most point to be rendered
     * @param near the nearest point to be rendered
     * @param far the furthest point to be rendered
     * @return the resulting matrix
     * @see <a href="https://github.com/TheCherno/Flappy/blob/master/src/com/thecherno/flappy/maths/Matrix4f.java">original</a>
     */
    public static Matrix4x4 orthographic(float left, float right, float bottom, float top, float near, float far) {
        Matrix4x4 result = identity();

        result.values[0 + 0 * numberOfRowsOrCollums] = 2.0f / (right - left);

        result.values[1 + 1 * numberOfRowsOrCollums] = 2.0f / (top - bottom);

        result.values[2 + 2 * numberOfRowsOrCollums] = 2.0f / (near - far);

        result.values[0 + 3 * numberOfRowsOrCollums] = (left + right) / (left - right);
        result.values[1 + 3 * numberOfRowsOrCollums] = (bottom + top) / (bottom - top);
        result.values[2 + 3 * numberOfRowsOrCollums] = (far + near) / (far - near);

        return result;
    }

    /**
     * Converts this instance to FloatBuffer
     * @author TheCherno
     * @return FloatBuffer representing this matrix instance
     * @see <a href="https://github.com/TheCherno/Flappy/blob/master/src/com/thecherno/flappy/maths/Matrix4f.java">original</a>
     */
    public FloatBuffer toFloatBuffer() {
        return BufferUtilities.createNewFloatBuffer(values);
    }
}
