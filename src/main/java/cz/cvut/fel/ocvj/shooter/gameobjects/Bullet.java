package cz.cvut.fel.ocvj.shooter.gameobjects;

import cz.cvut.fel.ocvj.shooter.math.Vector3;

public class Bullet extends Mobile {

    public Bullet(String pathToTexture, Vector3 position, float size, float speed, Vector3 initialDelta) {
        super(pathToTexture, position, size, speed);
        setRawDelta(initialDelta);
    }

    /**
     * Updates the bullet's position.
     * Is called together with the other GameObject's updates,
     * multiple times a second.
     */
    public void update() {
        this.move();
    }
}
