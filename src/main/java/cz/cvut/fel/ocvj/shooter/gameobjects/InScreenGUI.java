package cz.cvut.fel.ocvj.shooter.gameobjects;

import cz.cvut.fel.ocvj.shooter.math.Vector3;

public class InScreenGUI extends GUI{

    public InScreenGUI(String pathToTexture, Vector3 position, float size, Player player) {
        super(pathToTexture, position, size, player);
    }

    @Override
    protected float[] generateVertices(float size) {
        return new float[]{
                -size / 2.0f, -size / 2.0f, 0.0f, //vertex number 0, bottom left vertex
                -size / 2.0f, size / 2.0f, 0.0f, //vertex number 1, top left vertex
                size / 2.0f, size / 2.0f, 0.0f, //vertex number 2, top right vertex
                size / 2.0f, -size / 2.0f, 0.0f //vertex number 3, bottom right vertex
        };
    }
}
