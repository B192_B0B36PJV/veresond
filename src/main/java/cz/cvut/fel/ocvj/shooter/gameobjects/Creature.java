package cz.cvut.fel.ocvj.shooter.gameobjects;

import cz.cvut.fel.ocvj.shooter.math.Vector3;

public class Creature extends Mobile {
    private int hp;
    private long lastShotTime;
    private int reloadDuration;

    /**
     * Constructs a Creature instance
     * @param pathToTexture a relative path to the texture image
     * @param position starting position in the game world
     * @param size size of the instance in the game world
     * @param speed movement speed in the game world
     * @param hp health points indicating how many hits the creature can take before dying
     * @param reloadDuration interval in between shots
     */
    public Creature(String pathToTexture, Vector3 position, float size, float speed, int hp, int reloadDuration) {
        super(pathToTexture, position, size, speed);
        this.lastShotTime = System.currentTimeMillis();
        this.reloadDuration = reloadDuration;
        this.hp = hp;
    }

    public int getHp() { return this.hp; }
    public void setHp(int hp) { this.hp = hp; }

    /**
     * Checks if the Creature instance is ready to shoot.
     * Depending on the last shot taken and reloadDuration.
     * @return whether the instance is ready to shoot
     */
    public boolean canShoot() {
        long now = System.currentTimeMillis();
        if (now - lastShotTime > reloadDuration) {
            lastShotTime = now;
            return true;
        }
        return false;
    }

}
