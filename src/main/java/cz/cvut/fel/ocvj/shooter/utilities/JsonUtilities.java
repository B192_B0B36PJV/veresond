package cz.cvut.fel.ocvj.shooter.utilities;

import cz.cvut.fel.ocvj.shooter.math.Vector3;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Saving and loading files from and to JSON file
 */
public class JsonUtilities {
    private static FileWriter file;
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    /**
     * Save JSON to a file
     * @param obj JSONObject to be saved
     * @param name Name of the file
     */
    public static void saveJson(JSONObject obj, String name) {
        try {
            file = new FileWriter(name);
            file.write(obj.toJSONString());

        } catch (IOException e) {
            e.printStackTrace();

        } finally {
            try {
                file.flush();
                file.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

    }

    /**
     * Read a json file and return JSONObject
     * @param name name of the file
     * @return JSON with the data from the file
     */
    public static JSONObject readJson(String name) {
        JSONObject ret = null;
        try {
            ret = (JSONObject) new JSONParser().parse(new FileReader(name));
        } catch (IOException e) {
            LOGGER.log(Level.SEVERE, "Failed to load json", e);
        } catch (ParseException e) {
            LOGGER.log(Level.SEVERE, "Failed to load json", e);
        }
        return ret;
    }

    /**
     * Extracts map data from JSONObject
     * @param jsonObject original object
     * @return int array with the map data
     */
    public static int[] readMapArray(JSONObject jsonObject) {
        JSONArray jsonArray = (JSONArray) jsonObject.get("map");
        int[] array = new int[jsonArray.size()];
        for (int i = 0; i < jsonArray.size(); i++) {
            array[i] = ((Long) jsonArray.get(i)).intValue();
        }

        return  array;
    }
    /**
     * Extracts player position data from JSONObject
     * @param jsonObject original object
     * @return Vector3 with player position
     */
    public static Vector3 readPlayerPosition(JSONObject jsonObject) {
        JSONArray jsonArray = (JSONArray) jsonObject.get("player position");
        Vector3 ret = new Vector3(((Double)jsonArray.get(0)).floatValue(),((Double)jsonArray.get(1)).floatValue());
        return  ret;
    }
    /**
     * Extracts enemy positions data from JSONObject
     * @param jsonObject original object
     * @return Vector3 array with enemy positions
     */
    public static Vector3[] readEnemyPositions(JSONObject jsonObject) {
        JSONArray enemyPositions = (JSONArray) jsonObject.get("enemies");
        Vector3[] ret = new Vector3[enemyPositions.size()];
        for(int i = 0; i<enemyPositions.size(); i++)
        {
            JSONArray enemyPosition = (JSONArray)enemyPositions.get(i);
            ret[i] = new Vector3(((Double)enemyPosition.get(0)).floatValue(),((Double)enemyPosition.get(1)).floatValue());
        }

        return  ret;
    }

    /**
     * Saves current game status into JSONObject
     * @param map int array describing map
     * @param width width of the map
     * @param height height of the map
     * @param hasKey whether the player has key in his inventory
     * @param hp how much health does the player have
     * @param playerPosition
     * @param enemyPositions
     * @return JSONObject with the data
     */
    public static JSONObject generateJson(int[] map, int width, int height, Boolean hasKey, int hp, Vector3 playerPosition, Vector3[] enemyPositions)  {


        JSONObject ret = new JSONObject();
        JSONArray jsMap = new JSONArray();
        JSONArray jsEnemies = new JSONArray();
        JSONArray jsPlayerPosition = new JSONArray();



        for (int i = 0; i < map.length; i++) {
            jsMap.add(map[i]);
        }
        for (int i = 0; i < enemyPositions.length; i++) {
            JSONArray jsEnemyPosition = new JSONArray();
            jsEnemyPosition.add(enemyPositions[i].getX());
            jsEnemyPosition.add(enemyPositions[i].getY());
            jsEnemies.add(jsEnemyPosition);
        }
        jsPlayerPosition.add(playerPosition.getX());
        jsPlayerPosition.add(playerPosition.getY());


        ret.put("map", jsMap);
        ret.put("width", width);
        ret.put("height", height);
        ret.put("has key", hasKey);
        ret.put("hp",hp);
        ret.put("player position", jsPlayerPosition);
        ret.put("enemies", jsEnemies);
        return ret;
    }
}
