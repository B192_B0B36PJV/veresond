package cz.cvut.fel.ocvj.shooter.gameobjects;

import cz.cvut.fel.ocvj.shooter.math.Vector3;

public class Tile extends GameObject {
    public boolean isObstacle;
    int tileIndex;

    /**
     * Constructs an instance of Tile
     * @param pathToTexture a relative path to the texture image
     * @param position position in the game world
     * @param size size of the instance in the game world
     * @param isObstacle indicates whether the Player can pass through this Tile
     */
    public Tile(String pathToTexture, Vector3 position, float size, boolean isObstacle, int tileIndex) {
        super(pathToTexture, position, size);
        this.isObstacle = isObstacle;
        this.tileIndex = tileIndex;
    }
}
