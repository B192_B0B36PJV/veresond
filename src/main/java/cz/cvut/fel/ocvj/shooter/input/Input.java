//Inspired by https://github.com/TheCherno/Flappy/blob/master/src/com/thecherno/flappy/input/Input.java
package cz.cvut.fel.ocvj.shooter.input;

import org.lwjgl.glfw.GLFW;
import org.lwjgl.glfw.GLFWKeyCallback;

public class Input extends GLFWKeyCallback {

    private static boolean[] keys = new boolean[1000];

    /**
     * A callback that is called from Main whenever a key is pressed or released.
     * The key's state is saved inside the keys array.
     * See <a href="https://www.glfw.org/docs/3.3/group__input.html#ga0192a232a41e4e82948217c8ba94fdfd">reference</a>
     * for more info.
     * @param window The window that received the event
     * @param key The keyboard key that was pressed or released
     * @param scancode The system-specific scancode of the key
     * @param action GLFW_PRESS, GLFW_RELEASE or GLFW_REPEAT
     * @param mods Bit field describing which modifier keys were held down
     */
    public void invoke(long window, int key, int scancode, int action, int mods) {
        keys[key] = action != GLFW.GLFW_RELEASE;
    }

    /**
     * Returns whether the specified key is currently being pressed
     * @param key key code representing the key in question
     * @return whether the key is currently being pressed
     */
    public static boolean isKeyDown(int key) {
        return keys[key];
    }
}
