package cz.cvut.fel.ocvj.shooter.gameobjects;

import cz.cvut.fel.ocvj.shooter.collisions.Collisions;
import cz.cvut.fel.ocvj.shooter.math.Vector3;

public class Enemy extends Creature {
    /**
     * Constructs an instance of Enemy
     * @param pathToTexture a relative path to the texture image
     * @param position starting position in the game world
     * @param size size of the instance in the game world
     * @param speed movement speed in the game world
     * @param hp health points indicating how many hits the Enemy can take before dying
     * @param reloadDuration interval in between shots
     */
    public Enemy(String pathToTexture, Vector3 position, float size, float speed, int hp, int reloadDuration) {
        super(pathToTexture, position, size, speed, hp, reloadDuration);
    }

    /**
     * Updates the position of the instance in the game world,
     * following the player's position.
     * @param player instance of Player that the Enemy should follow
     */
    public void update(GameObject player) {
        Vector3 newRawDelta = new Vector3();
        if (Collisions.areTheyTouchingOnX(this, player)) {
            newRawDelta.setY(player.getPosition().getY() - this.position.getY());
        } else {
            newRawDelta.setX(player.getPosition().getX() - this.position.getX());
        }
        setRawDelta(newRawDelta);
        move();
    }
}
