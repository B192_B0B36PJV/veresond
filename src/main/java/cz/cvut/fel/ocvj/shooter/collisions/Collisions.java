package cz.cvut.fel.ocvj.shooter.collisions;

import cz.cvut.fel.ocvj.shooter.gameobjects.*;
import cz.cvut.fel.ocvj.shooter.math.Vector3;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Collisions {

    private final static int NO_PLAYER_COLLIDERS = -1;

    private Collisions() {}

    private static float getDistanceOnX(GameObject a, GameObject b) {
        return Math.abs(a.getPosition().getX() - b.getPosition().getX());
    }

    private static float getDistanceOnY(GameObject a, GameObject b) {
        return Math.abs(a.getPosition().getY() - b.getPosition().getY());
    }

    /**
     * Returns whether two objects are overlapping (position + size)
     * on the horizontal axis.
     * @param a first object
     * @param b second object
     * @return whether the two objects are overlapping on the x axis
     */
    public static boolean areTheyTouchingOnX(GameObject a, GameObject b) {
        return getDistanceOnX(a, b) <= a.getSize() / 2 + b.getSize() / 2;
    }

    /**
     * Returns whether two objects are overlapping (position + size)
     * on the vertical axis.
     * @param a first object
     * @param b second object
     * @return whether the two objects are overlapping on the y axis
     */
    public static boolean areTheyTouchingOnY(GameObject a, GameObject b) {
        return getDistanceOnY(a, b) <= a.getSize() / 2 + b.getSize() / 2;
    }

    /**
     * Returns whether two objects are touching.
     * @param a first object
     * @param b second object
     * @return whether two objects are touching
     */
    public static boolean areTheyTouching(GameObject a, GameObject b) {
        return areTheyTouchingOnX(a, b) && areTheyTouchingOnY(a, b);
    }

    private static Vector3 calculateCorrection(Player player, Tile tile) {
        Vector3 correction = new Vector3();
        float reserveSpace = 0.001f;
        if (areTheyTouching(player, tile)) {
            if (player.getRawDeltaX() > 0) {
                correction.setX(tile.getPosition().getX() - tile.getSize() / 2 - player.getPosition().getX() - player.getSize() / 2 - reserveSpace);
            } else if  (player.getRawDeltaX() < 0) {
                correction.setX(tile.getPosition().getX() + tile.getSize() / 2 - player.getPosition().getX() + player.getSize() / 2 + reserveSpace);
            } else if (player.getRawDeltaY() > 0) {
                correction.setY(tile.getPosition().getY() - tile.getSize() / 2 - player.getPosition().getY() - player.getSize() / 2 - reserveSpace);
            } else if (player.getRawDeltaY() < 0) {
                correction.setY(tile.getPosition().getY() + tile.getSize() / 2 - player.getPosition().getY() + player.getSize() / 2 + reserveSpace);
            }
        }
        return correction;
    }

    /**
     * Calculates the Vector by which the player needs to be moved
     * in order not to overlap with an unpassable obstacle (Tile).
     * @param player the Player instance that should be adjusted
     * @param tiles all the tiles that the Player can collide with
     * @return the Vector by which the player needs to be moved
     */
    public static Vector3 getCorrectionDelta(Player player, List<Tile> tiles) {
        Vector3 correctionDelta = new Vector3();
        for (Tile tile : tiles) {
            if (tile.isObstacle) {
                correctionDelta = calculateCorrection(player, tile);
                if (correctionDelta.getX() != 0 || correctionDelta.getY() != 0) {
                    return correctionDelta;
                }
            }
        }
        return correctionDelta;
    }

    /**
     * Returns a List of pairs of Enemies and Bullets that are colliding.
     * @param enemies List of enemies
     * @param bullets List of bullets
     * @return a List of pairs of Enemies and Bullets that are colliding
     */
    public static List<int[]> getColliderIndexes(List<Enemy> enemies, List<Bullet> bullets) {
        List<int[]> collidingPairs = new ArrayList<int[]>();
        for (int i = 0; i < enemies.size(); i++) {
            for (int j = 0; j < bullets.size(); j++) {
                if (areTheyTouching(enemies.get(i), bullets.get(j))) {
                    collidingPairs.add(new int[] {i, j});
                }
            }
        }
        return Collections.unmodifiableList(collidingPairs);
    }

    /**
     * Returns an index of the first Enemy in the list provided
     * that is colliding with the Player
     * @param player the instance of Player
     * @param enemies the list of Enemies
     * @return an index of the Colliding Enemy
     */
    public static int getPlayerToucherIndex(Player player, ArrayList<Enemy> enemies) {
        for (int i = 0; i < enemies.size(); i++) {
            if (areTheyTouching(enemies.get(i), player)) {
                return i;
            }
        }
        return NO_PLAYER_COLLIDERS;
    }
}
