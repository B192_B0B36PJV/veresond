package cz.cvut.fel.ocvj.shooter.gameobjects;

import com.thecherno.flappy.graphics.Shader;
import cz.cvut.fel.ocvj.shooter.Main;
import cz.cvut.fel.ocvj.shooter.collisions.Collisions;
import cz.cvut.fel.ocvj.shooter.input.Input;
import cz.cvut.fel.ocvj.shooter.math.Matrix4x4;
import cz.cvut.fel.ocvj.shooter.math.Vector3;
import cz.cvut.fel.ocvj.shooter.utilities.JsonUtilities;
import org.json.simple.JSONObject;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ThreadLocalRandom;

import static org.lwjgl.glfw.GLFW.*;

public class World {
    private static final float ENEMY_SPEED = 0.02f;
    private static Boolean isGameRunning = false;
    private static World world = null;
    private static int width;
    private static int height;
    private static float tileSize = 1.25f;
    private static int[] map;

    /**
     * List of tiles in the game world
     */
    public static List<Tile> tiles;

    private Player player;

    private Boolean isGameOver = false;
    private Boolean isTimeForRestart = false;

    private Tile key,chest;
    private List<Tile> potions = new ArrayList<Tile>();

    private GUI start, quit, gameover, victory, heart1, heart2, heart3, keyGUI;

    public Boolean getIsTimeForRestart() {
        return isTimeForRestart;
    }

    public Player getPlayer() {
        return player;
    }

    private ArrayList<Enemy> enemies = new ArrayList<Enemy>();
    private ArrayList<Bullet> bullets = new ArrayList<Bullet>();
    private ArrayList<GUI> guis = new ArrayList<GUI>();

    private World(String path_to_map) {
        JSONObject save = JsonUtilities.readJson(path_to_map);

       player = new Player("textures/player.png", JsonUtilities.readPlayerPosition(save), 1.5f, 0.08f, ((Long)save.get("hp")).intValue(), 500);
       player.setHasKey((Boolean) save.get("has key"));
       Vector3[] enemyPositions = JsonUtilities.readEnemyPositions(save);
       for(int i =0; i< enemyPositions.length;i++)
        {
            enemies.add(new Enemy("textures/bat01.png", enemyPositions[i], 2f, ENEMY_SPEED, 100, 500));
        }

       guis.add(start = new FullScreenGUI("textures/forest_start.png", new Vector3(0f,0f,0.7f), player));
       guis.add(quit = new FullScreenGUI("textures/forest_quit.png", new Vector3(0f,0f,0.6f), player));
       guis.add(victory = new FullScreenGUI("textures/forest_victory.png", new Vector3(0f,0f,0.6f), player));
       guis.add(gameover = new FullScreenGUI("textures/forest_gameover.png", new Vector3(0f,0f,0.6f), player));
       guis.add(heart1 = new InScreenGUI("textures/heart.png", new Vector3(-9.25f,4.85f,0.6f),1f, player));
       guis.add(heart2 = new InScreenGUI("textures/heart.png", new Vector3(-8f,4.85f,0.6f),1f, player));
       guis.add(heart3 = new InScreenGUI("textures/heart.png", new Vector3(-6.75f,4.85f,0.6f),1f, player));
       guis.add(keyGUI = new InScreenGUI("textures/key.png", new Vector3(8.25f,4.85f,0.6f),1f, player));
       victory.setActive(false);
       gameover.setActive(false);
       keyGUI.setActive(false);
       updateHealthGUI();
       updateKeyGUI();
    }

    /**
     * Creates an instance of World if there is none and returns it
     * @return an instance of World
     */
    public static World getWorld(String mapName)
    {
        if (world == null)
        {
            world = new World(mapName);
            world.createTiles(mapName);
        }

        return world;
    }

    /**
     * Returns a reset world in its default state
     * @return new game world
     */
    public static World resetWorld(String mapName)
    {

        world= new World(mapName);
        world.createTiles(mapName);

        return world;
    }

    private void createTiles(String path_to_map)
    {
        JSONObject save = JsonUtilities.readJson(path_to_map);
        map = JsonUtilities.readMapArray(save);
        width = ((Long)save.get("width")).intValue();
        height = ((Long)save.get("height")).intValue();
        tiles = new ArrayList<Tile>(width*height);

        for(int h = 0; h<height; h++)
        {
            for(int w =0; w<width; w++)
            {
                int tileIndex = h*width+w;
                int tileValue = map[tileIndex];
                Vector3 tilePosition = new Vector3(
                        w * tileSize - (width * tileSize / 2) + tileSize / 2,
                        h * tileSize - (height * tileSize / 2) + tileSize / 2, -0.1f
                );
                if (tileValue == -3) { // Key
                    Tile potion = new Tile("textures/health_potion.png", tilePosition, tileSize, false, tileIndex);
                    potions.add(potion);
                    tiles.add(potion);
                    generateRandomGrass(tilePosition, tileIndex);
                }else if (tileValue == -2) { // Key
                    key = new Tile("textures/key.png", tilePosition, tileSize, false, tileIndex);
                    tiles.add(key);
                    generateRandomGrass(tilePosition, tileIndex);
                } else if(tileValue == -1) { // Chest
                    chest = new Tile("textures/chest.png", tilePosition, tileSize, false, tileIndex);
                    tiles.add(chest);
                    generateRandomGrass(tilePosition, tileIndex);
                } else if (tileValue == 0) { // Grass
                        generateRandomGrass(tilePosition, tileIndex);
                } else if (tileValue == 5){ // Unpassable water
                    String path = String.format("textures/water_%d.png", tileValue);
                    tiles.add(new Tile(path, tilePosition, tileSize, true, tileIndex));
                } else { // Passable water
                    String path = String.format("textures/water_%d.png", tileValue);
                    tiles.add(new Tile(path, tilePosition, tileSize, false, tileIndex));
                }
            }
        }
    }

    private void generateRandomGrass(Vector3 tilePosition, int tileIndex) {
        int grassNum = ThreadLocalRandom.current().nextInt(0, 3);
        String path = String.format("textures/grass_%d.png", grassNum);
        tiles.add(new Tile(path, tilePosition, tileSize, false, tileIndex));
    }

    private void spawnBullet() {
        Vector3 position = new Vector3(player.position.getX(), player.position.getY());
        bullets.add(new Bullet("textures/bullet.png", position, 0.25f, 0.3f, player.direction));
    }

    /**
     * Updates all the GameObjects in the gameWorld,
     * checks collisions and adjusts positions of GameObjects as neccessary
     * @throws InterruptedException
     */
    public void update() throws InterruptedException {
        if(Input.isKeyDown(GLFW_KEY_S))
        {
            save();
        }
        if(Input.isKeyDown(GLFW_KEY_L))
        {
            load();
        }
        if(!potions.isEmpty())
        {
            boolean pickedPotion = false;
            for (int i = 0; i < potions.size(); i++) {
                if (Collisions.areTheyTouching(player, potions.get(i))) {
                    tiles.remove(potions.get(i));
                    potions.remove(i);
                    player.setHp(Math.min(player.getHp() + 1, player.getMaxHealth()));
                    updateHealthGUI();
                    break;
                }
            }
        }

        if(player.getHasKey())
        {

            if(Collisions.areTheyTouching(player,chest))
            {
                victory.setActive(true);
                isGameRunning=false;
                isGameOver = true;
            }
        }
        else{
            if(Collisions.areTheyTouching(player,key))
            {
                keyGUI.setActive(true);
                player.setHasKey(true);
                tiles.remove(key);
            }
        }
        if(isGameOver)
        {
            if(Input.isKeyDown(GLFW_KEY_ENTER))
            {
                isTimeForRestart = true;
                Main.getThread().sleep(100);
            }
        }
        if(!isGameRunning && !isGameOver) {


            if(Input.isKeyDown(GLFW_KEY_UP) || Input.isKeyDown(GLFW_KEY_DOWN))
            {
                guis.get(0).setActive(!guis.get(0).getActive());
                Main.getThread().sleep(300);
            }
            if(Input.isKeyDown(GLFW_KEY_ENTER))
            {
                if(!guis.get(0).getActive())
                {
                    Main.setRunning(false);
                }
                start.setActive(false);
                quit.setActive(false);
                isGameRunning = true;
            }
        }
        // Call update on all present gameObjects
        else {

            player.update();
            if (player.isShooting()) {
                spawnBullet();
            }

            for (Bullet b : bullets) {
                Thread newThread = new Thread(() -> {
                    b.update();
                });
                newThread.start();
            }
            for (Enemy e : enemies) {
                Thread newThread = new Thread(() -> {
                    e.update(player);
                });
                newThread.start();
            }
            // Check for collisions and adjust positions of gameObjects
            List<int[]> shotPairsIndexes;
            shotPairsIndexes = Collisions.getColliderIndexes(enemies, bullets);
            for (int[] i : shotPairsIndexes) {
                enemies.remove(i[0]);
                bullets.remove(i[1]);
            }
            int playerToucherIndex = Collisions.getPlayerToucherIndex(player, enemies);
            if (playerToucherIndex != -1) {
                player.setHp(player.getHp() - 1);
                updateHealthGUI();
                enemies.remove(playerToucherIndex);
            }

            Vector3 correctionDelta = Collisions.getCorrectionDelta(player, tiles);
            player.position.setX(player.position.getX() + correctionDelta.getX());
            player.position.setY(player.position.getY() + correctionDelta.getY());



        }
        for (GUI g : guis) {
            g.update();
        }
        Shader.getGameobjectShader().setUniformMat4f("vw_matrix", Matrix4x4.translationFromVector(Vector3.multiply(player.position,-1) ));
    }

    /**
     * Calls render on all GameObjects in the game world
     */
    public void render() {
        player.render();
        for (Enemy e : enemies) {
            e.render();
        }

        for (GUI g : guis) {
            g.render();
        }

        for (Tile t : tiles) {
            t.render();
        }

        for (Bullet b : bullets) {
            b.render();
        }

    }
    void updateKeyGUI(){
        if(player.getHasKey())
        {
            keyGUI.setActive(true);
        }
        else{
            keyGUI.setActive(false);
        }
    }
    void updateHealthGUI()
    {
        if (player.getHp() <= 0) {
            heart1.setActive(false);
            gameover.setActive(true);
            isGameRunning=false;
            isGameOver = true;
        } else if (player.getHp() == 1){
            heart1.setActive(true);
            heart2.setActive(false);
            heart3.setActive(false);
        } else if (player.getHp() == 2){
            heart1.setActive(true);
            heart2.setActive(true);
            heart3.setActive(false);
        } else if (player.getHp() == 3){
            heart1.setActive(true);
            heart2.setActive(true);
            heart3.setActive(true);
        }
    }
    private void save(){
        int[] newMap = new int[map.length];
        for (int i = 0; i < map.length; i++) {
            if (map[i] == -3) { // Potion
                newMap[i] = 0;
                for (Tile potion : potions) {
                    if (potion.tileIndex == i) {
                        newMap[i] = -3;
                        break;
                    }
                }
            } else {
                newMap[i] = map[i];
            }
        }
        Boolean hasKey= player.getHasKey();
        int hp = player.getHp();
        Vector3 playerPosition = player.getPosition();
        Vector3[] enemyPositions = new Vector3[enemies.size()];
        for(int i = 0; i<enemyPositions.length; i++)
        {
            enemyPositions[i]=enemies.get(i).getPosition();
        }
        JsonUtilities.saveJson(JsonUtilities.generateJson(newMap,20,20,hasKey, hp,playerPosition,enemyPositions),"save.json");
        this.map = newMap;
    }
    private void load(){
        isGameRunning=false;
        isTimeForRestart = true;
    }

    /**
     * Checks if key is still in the game
     */
    private int existKey()
    {
        if(tiles.contains(key))
            return 1;
        else return 0;

    }

}
