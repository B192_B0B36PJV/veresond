//Inspired by https://github.com/TheCherno/Flappy/blob/master/src/com/thecherno/flappy/utils/ShaderUtils.java

package cz.cvut.fel.ocvj.shooter.utilities;

import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

import static org.lwjgl.opengl.GL20.*;

/**
 * Loading and compiling shaders
 * Inspired by TheCherno
 * @see <a href="https://github.com/TheCherno/Flappy/blob/master/src/com/thecherno/flappy/utils/BufferUtils.java">original</a>
 */
public class ShaderUtilities {
    private final static Logger LOGGER = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    /**
     * Loads a Shader file as a string
     * @param pathToFile path to a Shader
     * @return long string with the shader
     */
    public static String loadShaderAsString(String pathToFile) {
        String shaderAsString = "";
        ClassLoader classloader = Thread.currentThread().getContextClassLoader();
        InputStream is = classloader.getResourceAsStream(pathToFile);
        //Creating a Scanner object
        try (Scanner scanner = new Scanner(is, StandardCharsets.UTF_8.name())) {
            shaderAsString = scanner.useDelimiter("\\A").next();
        }



        return shaderAsString;
    }

    /**
     * Compiling the shader
     * @param pathToVertexShader
     * @param pathToFragmentShader
     * @return the ID of the Shader program running on GPU
     */
    public static int generateShader(String pathToVertexShader, String pathToFragmentShader){
        int vertexShaderID = glCreateShader(GL_VERTEX_SHADER);
        int fragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

        String vertexShaderString = loadShaderAsString(pathToVertexShader);
        String fragmentShaderString = loadShaderAsString(pathToFragmentShader);

        glShaderSource(vertexShaderID,vertexShaderString);
        glShaderSource(fragmentShaderID,fragmentShaderString);

        glCompileShader(vertexShaderID);
        glCompileShader(fragmentShaderID);

        if(glGetShaderi(vertexShaderID,GL_COMPILE_STATUS) == GL_FALSE){
            LOGGER.log(Level.SEVERE, "Failed to compile vertex shader", getLogInfo(vertexShaderID));
            return -1;
        }

        if(glGetShaderi(fragmentShaderID,GL_COMPILE_STATUS) == GL_FALSE){
            LOGGER.log(Level.SEVERE, "Failed to compile fragment shader", getLogInfo(fragmentShaderID));
            return -1;
        }

        int programID = glCreateProgram();

        glAttachShader(programID, vertexShaderID);
        glAttachShader(programID, fragmentShaderID);

        glLinkProgram(programID);
        glValidateProgram(programID);

        glDeleteShader(vertexShaderID);
        glDeleteShader(fragmentShaderID);

        return programID;
    }

    /**
     * Get the error info about compilation of the shaders
     * @param obj programID
     * @return error message
     */
    public static String getLogInfo(int obj) {
        return glGetShaderInfoLog(obj, glGetShaderi(obj, GL_INFO_LOG_LENGTH));
    }
}
