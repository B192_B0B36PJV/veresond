package cz.cvut.fel.ocvj.shooter.gameobjects;

import cz.cvut.fel.ocvj.shooter.math.Vector3;

public class FullScreenGUI extends GUI {
    public FullScreenGUI(String pathToTexture, Vector3 position, Player player) {
        super(pathToTexture, position, 1f, player);
    }

    //Vertices normally create a square, this we need a 16:9 rectangle, which fills the screen
    @Override
    public float[] generateVertices(float size) {
        return new float[]{
                -10.0f, -10.0f * 9.0f / 16.0f, 0.0f, //vertex number 0, bottom left vertex
                -10.0f, 10.0f * 9.0f / 16.0f, 0.0f, //vertex number 1, top left vertex
                10.0f, 10.0f * 9.0f / 16.0f, 0.0f, //vertex number 2, top right vertex
                10.0f, -10.0f * 9.0f / 16.0f, 0.0f //vertex number 3, bottom right vertex
        };
    }
}
