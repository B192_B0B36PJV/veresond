package cz.cvut.fel.ocvj.shooter.math;

import junit.framework.TestCase;

import static org.junit.Assert.assertArrayEquals;

public class Matrix4x4Test extends TestCase {
    public void testMultiply() {
        Matrix4x4 a = new Matrix4x4(new float[]{1f, 2f, 3f, 4f, 5f, 6f, 7f, 8f, 9f, 10f, 11f, 12f, 13f, 14f, 15f, 16f});
        Matrix4x4 b = new Matrix4x4(new float[]{16f, 15f, 14f, 13f, 12f, 11f, 10f, 9f, 8f, 7f, 6f, 5f, 4f, 3f, 2f, 1f});

        Matrix4x4 expectedResult = new Matrix4x4(new float[]{386f, 444f, 502f, 560f, 274f, 316f, 358f, 400f, 162f, 188f, 214f, 240f, 50f, 60f, 70f, 80f});
        Matrix4x4 result = a.multiply(b);
        assertArrayEquals(expectedResult.getValues(), result.getValues(), 0.1f);
    }

    public void testSquareIdentity() {
        Matrix4x4 identity = Matrix4x4.identity();
        Matrix4x4 identitySquared = Matrix4x4.identity().multiply(Matrix4x4.identity());
        assertArrayEquals(identity.getValues(), identitySquared.getValues(), 0.1f);
    }

    public void testTranslationFromVector() {
        Vector3 vector = new Vector3(1f,2f,3f);
        Matrix4x4 expected = new Matrix4x4(new float[]{1f, 0f, 0f, 0f, 0f, 1f, 0f, 0f, 0f, 0f, 1f, 0f, 1f, 2f, 3f, 1f});
        Matrix4x4 result = Matrix4x4.translationFromVector(vector);
        assertArrayEquals(expected.getValues(), result.getValues(),0.1f);
    }

    public void testRotationFromVector() {
        Vector3 vector = new Vector3(1f,1f);
        Matrix4x4 expected = new Matrix4x4(new float[]{
                1f, -1f, 0f, 0f,
                1f, 1f, 0f, 0f,
                0f, 0f, 1f, 0f,
                0f, 0f, 0f, 1f});
        Matrix4x4 result = Matrix4x4.identity().rotationFromVector(vector);
        assertArrayEquals(expected.getValues(), result.getValues(),0.1f);
    }


}