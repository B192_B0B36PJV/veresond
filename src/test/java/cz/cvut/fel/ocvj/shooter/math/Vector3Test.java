package cz.cvut.fel.ocvj.shooter.math;

import junit.framework.TestCase;

public class Vector3Test extends TestCase {
    public void testNormalize() {
        Vector3 v3 = new Vector3(10f,-10f);
        v3.normalize();
        Vector3 expectedNormalized = new Vector3(1f, -1f);
        assertEquals(expectedNormalized, v3);
    }

    public void testAdd() {
        Vector3 a = new Vector3(5f, 3f);
        Vector3 b = new Vector3(4f, 2f);
        Vector3 expected = new Vector3(9f, 5f);
        assertEquals(expected, Vector3.add(a, b));
    }

    public void testMultiply() {
        Vector3 a = new Vector3(2f, 1f);
        float m = 3f;
        Vector3 expected = new Vector3(6f, 3f);
        assertEquals(expected, Vector3.multiply(a, m));
    }
}